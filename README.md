# INSTALL #

```
git clone git@bitbucket.org:zero13cool/devhub.git devhub
cd devhub
virtualenv venv
. venv/bin/activate

pip install -r requirements.test.txt

./manage.py syncdb
./manage.py loadtestdata auth.User:10
./manage.py loadtestdata blog.BlogPost:20
```

Запуск тестов:

```
./manage.py test blog
```