# coding: utf-8
from django.template import Library
from django.core.urlresolvers import reverse

register = Library()


@register.inclusion_tag('blog/helpers/read_unread_link.html', takes_context=True)
def render_read_unread_link(context, blog):
    """ Тэг-хэлпер для отрисовки ссылки "Начать читать"/"Прекратить читать"
    какой-то блог.
    """
    request = context['request']

    link_title = u'Начать читать'
    link_url = 'start_read_blog'

    if not request.user.is_authenticated():
        return locals()

    my_blog = request.user.blog
    is_my_blog = my_blog == blog
    if is_my_blog:
        return locals()

    already_read = my_blog.observed_blogs.filter(id=blog.pk).exists()
    if already_read:
        link_title = u'Перестать читать'
        link_url = 'stop_read_blog'

    return locals()
