# coding: utf-8
from django.test import TestCase
from django.conf import settings
from django.db.models.loading import get_model


class LoginedTestCase(TestCase):
    """ Общий тесткес, создающий и авторизующие пользователя.
    """
    def setUp(self):
        User = get_model(settings.AUTH_USER_MODEL)
        user_pass = 'qweqwe'
        username = 'superuser'
        email = 'test@test.com'
        password = 'test'
        self.user = User.objects.create_superuser(username, email, password)
        self.client.login(username=username, password=password)
