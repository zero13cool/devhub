# coding: utf-8
from blog.models import Blog
from blog.tests.base import LoginedTestCase


class BlogTestCase(LoginedTestCase):
    """ Тесты для модели блога.
    """

    def test_autocreate(self):
        """ Проверяем автосоздание блога после создания нового пользователя.
        """
        self.assertIsInstance(self.user.blog, Blog)
