# coding: utf-8
from django.test import TestCase
from django.core.urlresolvers import reverse

from autofixture import AutoFixture
from blog.models import Blog, BlogPost

from blog.tests.base import LoginedTestCase


class BlogPostTestCase(LoginedTestCase):
    """ Тесты для вьюхи постов.
    """
    def test_redirect(self):
        """ Проверяем возможность опубликовать пост.
        """
        response = self.client.get('/', follow=True)
        self.assertEqual(len(response.redirect_chain), 1)
        self.assertEqual(response.status_code, 200)

        # Редирект для анонимуса
        self.client.logout()
        response = self.client.get('/', follow=True)
        self.assertEqual(len(response.redirect_chain), 2)
        self.assertEqual(response.status_code, 200)

    def test_post_add(self):
        """ Тестируем добавление поста.
        """
        before = BlogPost.objects.count()

        response = self.client.post(reverse('add_post'), {
            'title': 'asdsad', 'body': 'asda'}, follow=True)
        self.assertEqual(len(response.redirect_chain), 1)
        self.assertEqual(response.status_code, 200)
        self.assertIn(u'добавлен', response.content.decode('utf-8'))

        after = BlogPost.objects.count()
        self.assertTrue(after > before)


class BlogTestCase(LoginedTestCase):
    """ Тесты для вью блогов.
    """
    def setUp(self):
        super(BlogTestCase, self).setUp()
        self.users = AutoFixture(self.user.__class__).create(10)
        self.blogs = [u.blog for u in self.users]
        self.my_blog = self.user.blog
        self.other_blog = self.blogs[0]

    @property
    def observed_blogs_count(self):
        return self.my_blog.observed_blogs.count()

    def common_response(self, url_name):
        return self.client.get(
            reverse(url_name, kwargs={'pk': self.other_blog.pk}),
            follow=True)

    def test_list_view(self):
        """ Тестируем отображение списка блогов.
        """
        response = self.client.get(reverse('blog_list'))
        self.assertEqual(response.status_code, 200)

        blogs = response.context['object_list']
        self.assertEqual(len(blogs), Blog.objects.count())

        for blog in self.blogs:
            self.assertTrue(blog in blogs)

    def test_read_blog(self):
        """ Тестируем возможность подписаться.
        """
        self.assertEqual(self.observed_blogs_count, 0)

        response = self.common_response('start_read_blog')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.observed_blogs_count, 1)

    def test_stop_read_blog(self):
        """ Тестируем отписку от блога.
        """
        response = self.common_response('start_read_blog')
        response = self.common_response('stop_read_blog')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.observed_blogs_count, 0)
