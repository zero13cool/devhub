# coding: utf-8
from django.contrib import admin

from blog.models import Blog, BlogPost

admin.site.register([Blog, BlogPost])
