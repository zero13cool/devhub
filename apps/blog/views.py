# coding: utf-8
from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.shortcuts import redirect, get_object_or_404
from django.contrib import messages
from django.views.decorators.http import require_http_methods
from django.core.urlresolvers import reverse
from django.db.models import Q

from blog.models import Blog, BlogPost
from blog.forms import BlogPostForm


ACTIONS = ADD, REMOVE = range(2)


class BlogListView(ListView):
    """ Список блогов.
    """
    model = Blog


class MyBlogDetailView(DetailView):
    """ Страница моего блога.
    """
    model = Blog

    def get_object(self):
        return self.request.user.blog

    def get_context_data(self, *args, **kwargs):
        ctx = super(MyBlogDetailView, self).get_context_data(*args, **kwargs)
        ctx['form'] = BlogPostForm()
        ctx['feed'] = BlogPost.objects.filter(
            Q(blog=self.object) |
            Q(blog__id__in=self.object.observed_blogs.all())
        )
        return ctx


class BlogDetailView(DetailView):
    """ Страница вражеского блога.
    """
    model = Blog

    def get_context_data(self, *args, **kwargs):
        ctx = super(BlogDetailView, self).get_context_data(*args, **kwargs)
        ctx['feed'] = self.object.posts.all()
        return ctx


class BlogPostDetailView(DetailView):
    """ Страница поста из блога.
    """
    model = BlogPost


@require_http_methods(['POST'])
def add_post(request):
    """ Простое добавление поста в свою ленту.
    """
    form = BlogPostForm(request.POST)
    if form.is_valid():
        form.instance.blog = request.user.blog
        form.save()
        messages.info(request, u'Пост успешно добавлен.')
    else:
        messages.error(request, u'Некорретные значения в полях формы.')

    return redirect(reverse('my_blog'))


@require_http_methods(['GET'])
def start_read(request, pk):
    """ Начать читать чей-то блог.
    """
    return common_action_view(request, pk, ADD)


@require_http_methods(['GET'])
def stop_read(request, pk):
    """ Перестать читать чей-то блог.
    """
    return common_action_view(request, pk, REMOVE)


def common_action_view(request, pk, action):
    """ Общая логика для подписки / отписки на чей-то блог.
    """
    next = request.GET.get('next', '/')
    my_blog = request.user.blog
    blog = get_object_or_404(Blog, pk=pk)

    getattr(my_blog.observed_blogs, ['add', 'remove'][action])(blog)

    return redirect(next)
