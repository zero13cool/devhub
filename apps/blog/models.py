# coding: utf-8
from urlparse import urljoin

from django.conf import settings
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.utils.encoding import python_2_unicode_compatible
from django.core.urlresolvers import reverse
from django.core.mail import send_mail


@python_2_unicode_compatible
class Blog(models.Model):
    """ Блог пользователя. Связан с пользователем через OneToOne отношение.
    Для решения задачи достаточно было привязки постов к пользователю.
    Однако в реальных условиях наличие сущности `Блог` - более расширяемое
    решение.
    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL)

    observed_blogs = models.ManyToManyField(
        'self', symmetrical=False, related_name='observers')

    def __str__(self):
        return u'Блог пользователя: {}'.format(self.user)

    class Meta:
        verbose_name = u'Блог'
        verbose_name_plural = u'Блоги'

    def get_absolute_url(self):
        return reverse('blog_detail', kwargs={'pk': self.pk})


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_blog(sender, instance, created, *args, **kwargs):
    if created:
        Blog(user=instance).save()


@python_2_unicode_compatible
class BlogPost(models.Model):
    """ Поста пользователя в блога.
    """
    blog = models.ForeignKey(Blog, related_name='posts')
    dt = models.DateTimeField(u'создан', auto_now_add=True, db_index=True)
    title = models.CharField(u'заголовок', max_length=225)
    body = models.TextField(u'текст')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = u'Запись в блоге'
        verbose_name_plural = u'Записи в блоге'
        ordering = ['-dt']

    def get_absolute_url(self):
        return reverse('blog_post_detail', kwargs={'pk': self.pk})


@receiver(post_save, sender=BlogPost)
def send_notify(sender, instance, created, *args, **kwargs):
    if not created:
        return

    title = u'Новый пост в блоге: {}'.format(instance.blog)
    emails = instance.blog.observers.filter(user__email__isnull=False)\
                                    .values_list('user__email', flat=True)

    message = '\n'.join(
        [instance.body, urljoin(settings.HOST, instance.get_absolute_url())])

    send_mail(title, message, settings.DEFAULT_FROM, emails)
