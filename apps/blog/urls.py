# coding: utf-8
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from blog import views

lr = login_required(login_url='/admin/login/')

urlpatterns = patterns(
    '',
    url(
        '^my/$',
        lr(views.MyBlogDetailView.as_view()),
        name='my_blog'
    ),
    url(
        '^my/add/$',
        lr(views.add_post),
        name='add_post'
    ),
    url(
        '^list/$',
        views.BlogListView.as_view(),
        name='blog_list'
    ),
    url(
        '^post/(?P<pk>\d+)/$',
        views.BlogPostDetailView.as_view(),
        name='blog_post_detail'
    ),

    url(
        '^(?P<pk>\d+)/$',
        views.BlogDetailView.as_view(),
        name='blog_detail'
    ),
    url(
        '^(?P<pk>\d+)/read/$',
        lr(views.start_read),
        name='start_read_blog'
    ),
    url(
        '^(?P<pk>\d+)/unread/$',
        lr(views.stop_read),
        name='stop_read_blog'
    ),
)
