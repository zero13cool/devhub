# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


def create_blogs(apps, schema_editor):
    Blog = apps.get_model('blog', 'Blog')
    User = apps.get_model(settings.AUTH_USER_MODEL)

    for user in User.objects.all():
        blog, _ = Blog.objects.get_or_create(user=user)


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_blogs,),
    ]
