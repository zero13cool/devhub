# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20141113_2019'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blogpost',
            options={'ordering': ['-dt'], 'verbose_name': '\u0417\u0430\u043f\u0438\u0441\u044c \u0432 \u0431\u043b\u043e\u0433\u0435', 'verbose_name_plural': '\u0417\u0430\u043f\u0438\u0441\u0438 \u0432 \u0431\u043b\u043e\u0433\u0435'},
        ),
        migrations.AlterField(
            model_name='blogpost',
            name='dt',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0441\u043e\u0437\u0434\u0430\u043d', db_index=True),
            preserve_default=True,
        ),
    ]
