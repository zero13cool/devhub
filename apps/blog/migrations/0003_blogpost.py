# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20141113_1632'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(auto_now_add=True, verbose_name='\u0441\u043e\u0437\u0434\u0430\u043d')),
                ('title', models.CharField(max_length=225, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('body', models.TextField(verbose_name='\u0442\u0435\u043a\u0441\u0442')),
                ('blog', models.ForeignKey(related_name='posts', to='blog.Blog')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043f\u0438\u0441\u044c \u0432 \u0431\u043b\u043e\u0433\u0435',
                'verbose_name_plural': '\u0417\u0430\u043f\u0438\u0441\u0438 \u0432 \u0431\u043b\u043e\u0433\u0435',
            },
            bases=(models.Model,),
        ),
    ]
