# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_blogpost'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='observed_blogs',
            field=models.ManyToManyField(related_name='observed_blogs_rel_+', to='blog.Blog'),
            preserve_default=True,
        ),
    ]
