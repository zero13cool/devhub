# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_blog_observed_blogs'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='observed_blogs',
            field=models.ManyToManyField(to='blog.Blog'),
            preserve_default=True,
        ),
    ]
