# coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import RedirectView


urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='/blog/my/', permanent=False)),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^blog/', include('blog.urls')),
)
